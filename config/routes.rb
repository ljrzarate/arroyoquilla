Miarroyo::Application.routes.draw do

  root to: "arroyos#index"
  devise_for :users, skip: [:registrations]

  resources :arroyos do
    collection do
      get 'get_points'
      post 'get_arroyo'
      get 'alternative'
    end
  end
  resources :points
  resources :sensors
  resources :gcm_users, only: [:create]
end
