class ArroyosController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :edit]

  def index
    if params[:range]
      @arroyos = Arroyo.by_date_range(params[:range])
    else
      @arroyos = Arroyo.find(:all, include: [:points, :sensors])
    end
    respond_to do |format|
      format.html
      format.json {render_for_api :basic_arroyo, json: @arroyos}
    end
  end

  def new
    @arroyo = Arroyo.new
  end

  def edit
    @arroyo = Arroyo.find(params[:id])
  end

  def update
      @arroyo = Arroyo.find(params[:id])
    if @arroyo.update_attributes(params[:arroyo])
      flash.now[:notice] = "Arroyo actuailzado correctamente"
      redirect_to(root_path)
    else
      flash.now[:error] = "No se puede actualizar el arroyo"
      render :edit
    end
  end

  def create
    @arroyo = Arroyo.new(params[:arroyo])
    if @arroyo.save
      redirect_to root_path
    else
      flash.now[:error] = "No se puede agregar el arroyo"
    end
  end

  def destroy
    @arroyo = Arroyo.find(params[:id])
    @arroyo.destroy
    redirect_to root_path
  end

  def get_points
    @resp = {}
    Arroyo.all.each_with_index do |arroyo, index|
      @resp["points_#{index}"] = arroyo.all_points
      @resp["sensors_#{index}"] = arroyo.all_sensors
    end
    @resp
    respond_to do |format|
      format.html
      format.json {render json: @resp}
      format.mobile {render json: @resp}
    end
  end

  def get_arroyo
    @resp = {}
    arroyo = Arroyo.find(params[:arroyo])
    @resp["points"] = arroyo.all_points
    if params[:sensor].present?
      @resp["sensors"] = Sensor.find(params[:sensor]).make
    else
      @resp["sensors"] = arroyo.all_sensors
    end
    @resp
    respond_to do |format|
      format.html
      format.json {render json: @resp}
      format.mobile {render json: @resp}
    end
  end

  def alternative
    @points = Point.alternative_points.near(params[:origin], 20).first(2)
    respond_to do |format|
      format.json {render json: @points}
    end
  end
end
