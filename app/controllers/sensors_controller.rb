class SensorsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :edit, :destroy]

  def index
    @sensors = Sensor.find(:all, order: "arroyo_id")
    respond_to do |format|
      format.html
      format.json {render_for_api :basic_sensor, json: @sensors}
    end
  end

  def new
    @sensor = Sensor.new
  end

  def create
    @sensor = Sensor.new(params[:sensor])
    if @sensor.save
      flash.now[:notice] = "Sensor added successfuly."
    else
      flash.now[:error] = "Could not add Sensor."
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    @sensor = Sensor.find(params[:id])
  end

  def update
    @sensor = Sensor.find(params[:id])
    if @sensor.update_attributes(params[:sensor])
      flash.now[:notice] = "sensor added successfuly."
    else
      flash.now[:error] = "could not update the sensor."
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def destroy
    @sensor = Sensor.find(params[:id])
    if @sensor.destroy
      redirect_to sensors_path, notice: "Sensor Deleted successfuly"
    else
      flash[:error] = @sensor.errors.full_messages.to_sentence
    end
  end
end
