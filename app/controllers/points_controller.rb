class PointsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :edit]

  def index
    @points = Point.find(:all, order: "arroyo_id")
    respond_to do |format|
      format.html
      format.json {render_for_api :basic_point, json: @points}
    end
  end

  def new
    @point = Point.new
  end

  def create
    @point = Point.new(params[:point])
    if @point.save
      flash.now[:notice] = "point added successfuly."
    else
      flash.now[:error] = "could not add the point."
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  # def edit
  #   @point = Point.find(params[:id])
  # end

  def destroy
    @point = Point.find(params[:id])
    if @point.destroy
      redirect_to points_path, notice: "Point Deleted successfuly"
    else
      flash[:error] = @point.errors.full_messages.to_sentence
    end
  end

end
