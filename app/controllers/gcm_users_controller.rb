class GcmUsersController < ApplicationController
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  def create
    @gcm_user = GcmUser.new(gcm_regid: params[:gcm_user])
    if @gcm_user.save
      respond_to do |format|
        format.json{render json: @gcm_user}
      end
    end
  end
end
