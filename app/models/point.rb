class Point < ActiveRecord::Base

  acts_as_api

  attr_accessible :arroyo_id, :latitude, :longitude, :sensor_id, :alternative

  belongs_to :arroyo
  belongs_to :sensor

  default_scope order('id ASC')

  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :arroyo, presence: true

  scope :normal_points, where(alternative: nil)
  scope :alternative_points, where(alternative: true)

  geocoded_by :address, latitude: :latitude, longitude: :longitude
  after_validation :geocode

  api_accessible :basic_point do |t|
    t.add :latitude
    t.add :longitude
    t.add :arroyo_id
    t.add :to_pair
  end

  def address
    [self.latitude, self.longitude]
  end

  def to_pair
    resp = []
    resp[0] = latitude
    resp[1] = longitude
    resp[2] = id
    resp[3] = sensor_id

    resp
  end
end
