class Sensor < ActiveRecord::Base

  include PushNotification
  include ReadSensorsXML
  acts_as_api

  attr_accessible :arroyo_id, :latitude, :longitude, :status, :external_id

  default_scope order('id ASC')

  belongs_to :arroyo
  has_many :points

  STATUS_OPTIONS = ["good", "bad", "medium"]

  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :status, presence: true
  validates :status, inclusion: {:in => STATUS_OPTIONS}

  before_save :refresh_google_maps
  #after_save :send_push_notification, :if => Proc.new {|obj| obj.status_changed? }

  api_accessible :basic_sensor do |t|
    t.add :latitude
    t.add :longitude
    t.add :status
    t.add :arroyo_id
    t.add :make
  end

  def make
    resp = []
    resp[0] = latitude
    resp[1] = longitude
    resp[2] = status
    resp[3] = id
    resp
  end

  def refresh_google_maps
    PrivatePub.publish_to("/points/new", "console.log('sensor updated');")
  end

  private
    def is_bad?
      self.status == 'bad' ? true : false
    end

    def is_good
      self.status == 'good' ? true : false
    end

    def is_medium
      self.status == 'medium' ? true : false
    end
end
