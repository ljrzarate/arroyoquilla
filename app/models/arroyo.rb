class Arroyo < ActiveRecord::Base

  acts_as_api

  has_many :sensors, dependent: :destroy
  has_many :points, dependent: :destroy

  attr_accessible :address,
                  :name,
                  :points_attributes

  scope :by_date_range, ->(range) { where("updated_at between ? and ?", range.split("|")[0].to_s.to_time, range.split("|")[1].to_s.to_time) }

  validates :name, presence: true

  api_accessible :basic_arroyo do |t|
    t.add :id
    t.add :address
    t.add :name
    t.add :all_points
    t.add :all_sensors
  end

  accepts_nested_attributes_for :points, allow_destroy: true
  accepts_nested_attributes_for :sensors, allow_destroy: true

  def all_points
    results = []
    self.points.each do |point|
      if !point.alternative
        results << point.to_pair
      end
    end
    results
  end

  def all_sensors
    results = []
    self.sensors.each do |sensor|
      results << sensor.make
    end
    results
  end
end
