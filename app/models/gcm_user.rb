class GcmUser < ActiveRecord::Base
  attr_accessible :email, :gcm_regid, :name

  validates :gcm_regid, presence: true

end
