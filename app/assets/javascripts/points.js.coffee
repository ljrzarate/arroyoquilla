# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  #########################
  # Creating the map
  #########################
  window.map_points = new GMaps(
      el: "#map"
      lat: 10.9907
      lng: -74.7955
      zoomControl: true
      zoomControlOpt:
        style: "SMALL"
        position: "TOP_LEFT"

      panControl: false
      streetViewControl: false
      mapTypeControl: false
      overviewMapControl: false,
      click: (e) ->
        $("#point_latitude").val(e.latLng.lat())
        $("#point_longitude").val(e.latLng.lng())
        map_points.addMarker(
          icon: 'http://google.com/mapfiles/ms/micons/lightblue.png'
          lat: e.latLng.lat()
          lng: e.latLng.lng()
          draggable: true
          drag: (e)->
            $("#point_latitude").val(e.latLng.lat())
            $("#point_longitude").val(e.latLng.lng())
        )
    )

  #########################
  # Function to change marker icon depending status of sensor
  #########################
  changeMarker = (data)->
    if data == "bad"
      'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    else if data == "good"
      'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    else if data == "medium"
      'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'

  #########################
  # draw points with array of points like gmaps.js needs
  #########################

  drawPoints = (points)->
    map_points.drawPolyline(
      path: points
      strokeColor: '#0080FF'
      strokeOpacity: 1
      strokeWeight: 10
    )

  addMarker = (markers)->
    $.each(markers, (index, value)->
      map_points.addMarker(
        lat: markers[index][0]
        lng: markers[index][1]
        infoWindow:
          content: markers[index][2]
        icon: changeMarker(markers[index][2])
      )
    )

  #########################
  # Function to draw a individual arroyo
  #########################
  get_sigle_arroyo = (arroyo_id)->
    $.ajax(
      url: "/arroyos/get_arroyo.json"
      type: "POST"
      dataType: "json"
      data:
        arroyo: arroyo_id
      success: (data)->
        drawPoints data['points']
        addMarker data['sensors']
    )

  #########################
  # Select a Arroyo and draw it to add more points
  #########################
  $("#point_arroyo_id").on "change", ->
    arroyo_id = $(this).val()
    map_points.removePolylines()
    map_points.removeMarkers()
    $("#point_latitude").val("")
    $("#point_longitude").val("")

    if arroyo_id == ""
      $("div.lat-lng").addClass("hidden")
    else
      $("div.lat-lng").removeClass("hidden")
      get_sigle_arroyo(arroyo_id)

  #########################
  # Load arroyo if its a edit page
  #########################
  if google.maps
    arroyo_id = $("#point_arroyo_id").val()
    get_sigle_arroyo(arroyo_id)
