#########################
# Function to change the marker depending of sensor status
# icons are on: https://sites.google.com/site/gmapsdevelopment/
#########################

window.changeMarker = (data)->

  if data == "bad"
    'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
  else if data == "good"
    'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
  else if data == "medium"
    'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'

#########################
# draw points with array of points like gmaps.js needs
#########################

window.drawPoints = (points)->
  map.drawPolyline(
    path: points
    strokeColor: '#4225fa'
    strokeOpacity: 1
    strokeWeight: 5
  )

window.addMarker = (markers)->
  $.each(markers, (index, value)->
    map.addMarker(
      lat: markers[index][0]
      lng: markers[index][1]
      infoWindow:
        content: markers[index][2]
      icon: changeMarker(markers[index][2])
    )
  )

window.get_all_arroyo = ->
  $.ajax(
    url: "/arroyos/get_points.json"
    type: "GET"
    dataType: "json"
    success: (data)->
      $.each(data, (index, value)->
        if index.indexOf("points") != -1
          drawPoints data[index]
        else
          addMarker data[index]
      )
    error: (data, status, errorThrown) ->
      console.log data
      console.log status
      console.log errorThrown
  )