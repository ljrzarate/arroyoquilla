$ ->
  #########################
  # Creating the map
  #########################
  window.map_sensor = new GMaps(
    el: "#map"
    lat: 10.9907
    lng: -74.7955
    zoomControl: true
    zoomControlOpt:
      style: "SMALL"
      position: "TOP_LEFT"

    panControl: false
    streetViewControl: false
    mapTypeControl: false
    overviewMapControl: false
  )

  drawPoints = (points)->
    map_sensor.drawPolyline(
      path: points
      strokeColor: '#0080FF'
      strokeOpacity: 0.6
      strokeWeight: 5
    )

  addMarker = (markers)->
    $.each(markers, (index, value)->
      map_sensor.addMarker(
        lat: markers[index][0]
        lng: markers[index][1]
        # infoWindow:
        #   content: markers[index][2]
        icon: window.changeMarker(markers[index][2])
        dblclick: (e) ->
          lat = this.getPosition().lat()
          long = this.getPosition().lng()
          this.setDraggable(true)
          $("#sensor_latitude").val(lat)
          $("#sensor_longitude").val(long)
        drag: (e)->
          lat = this.getPosition().lat()
          long = this.getPosition().lng()
          $("#sensor_latitude").val(lat)
          $("#sensor_longitude").val(long)

      )
    )

  get_sigle_arroyo = (arroyo_id, sensor_id = null)->
    console.log sensor_id
    $.ajax(
      url: "/arroyos/get_arroyo.json"
      type: "POST"
      dataType: "json"
      data:
        arroyo: arroyo_id
        sensor: sensor_id
      success: (data)->
        console.log data
        drawPoints data['points']
        map_sensor.addMarker(
          icon: 'http://google.com/mapfiles/ms/micons/lightblue.png'
          lat: data['sensors'][0]
          lng: data['sensors'][1]
          draggable: true
          drag: (e)->
            $("#sensor_latitude").val(e.latLng.lat())
            $("#sensor_longitude").val(e.latLng.lng())
        )
    )

  if google.maps
    arroyo = $("#sensor_arroyo_id")
    arroyo.attr("disabled", true)
    sensor = $("h5.subtitle").data("sensor-id")
    get_sigle_arroyo(arroyo.val(), sensor)