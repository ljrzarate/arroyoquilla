#####
# PrivatePub to subscript points creation channel to shown in real time points added
#####
PrivatePub.subscribe "/points/new", (data, channel) ->
  map.removePolylines()
  map.removeMarkers()
  get_all_arroyo()

jQuery ->

  #########################
  # Creating the map
  #########################
  clicks = 0
  window.map = new GMaps(
    el: "#map"
    lat: 10.9907
    lng: -74.7955
    click: (e) ->
      markerIcons = ["http://maps.google.com/mapfiles/marker_greenA.png", "http://maps.google.com/mapfiles/markerB.png"]
      if clicks < 2
        clicks++
        if clicks == 1
          $(".js-user-lat-origin").val(e.latLng.lat())
          $(".js-user-lng-origin").val(e.latLng.lng())
        else
          $(".js-get-alternative-route").removeClass("hidden")
          $(".js-user-lat-destination").val(e.latLng.lat())
          $(".js-user-lng-destination").val(e.latLng.lng())

        map.addMarker(
          icon: markerIcons[clicks-1]
          lat: e.latLng.lat()
          lng: e.latLng.lng()
          draggable: true
          drag: (e)->
            if this.numIndicator == 1
              $(".js-user-lat-origin").val(e.latLng.lat())
              $(".js-user-lng-origin").val(e.latLng.lng())
            else
              $(".js-user-lat-destination").val(e.latLng.lat())
              $(".js-user-lng-destination").val(e.latLng.lng())
        )
  )

  #########################
  # if google.maps is loaded execute ajax to load points
  #########################

  if google.maps
    map.removePolylines()
    map.removeMarkers()
    window.get_all_arroyo()

  #################################
  # Filter by arroyo on index page
  ################################
  $("#arroyo_arroyo_id").on "change", ->
    map.removePolylines()
    map.removeMarkers()
    arroyo_id = $(this).val()
    if arroyo_id == ""
      get_all_arroyo()
    else
      $.ajax(
        url: "/arroyos/get_arroyo.json"
        type: "POST"
        dataType: "json"
        data:
          arroyo: arroyo_id
        success: (data)->
          drawPoints data['points']
          addMarker data['sensors']
      )

  ###############
  # Show list of arroyos on index page
  ###############
  $("a#list-arroyos").on "click", ->
    if $(this).text() == "Listar todos los arroyos"
      $("#arroyo_arroyo_id").fadeIn("fast")
      $("div#map").fadeOut("fast")
      $("div#list-arroyos").removeClass("hidden")
      $("#legend").fadeOut("fast")
      $(this).text("Mostrar Mapa")
    else
      $("#arroyo_arroyo_id").fadeIn("fast")
      $("div#map").fadeIn("fast")
      $("div#list-arroyos").addClass("hidden")
      $("#legend").fadeIn("fast")
      $(this).text("Listar todos los arroyos")

  ###############
  # Get alternative routes for a direction
  ###############
  getUserLatLng = (lat_selector, lng_selector)->
    [$(lat_selector).val(), $(lng_selector).val()]

  calculateRoutes = ->
    origin = getUserLatLng(".js-user-lat-origin", ".js-user-lng-origin")
    destination = getUserLatLng(".js-user-lat-destination", ".js-user-lng-destination")
    $.ajax(
      url: "/arroyos/alternative.json"
      data: {
        origin: origin
        destination: destination
      }
      success: (data)->
        # [ [-12.044012922866312, -77.02470665341184], [-12.05449279282314, -77.03024273281858] ]]
        console.log(data)
        pointA = origin
        pointB = destination
        pointAlternative = [ data["latitude"], data["longitude"] ]
        path = [pointA, pointB, pointAlternative]
        $.each(data, (index, value)->
          pointAlternative = [ data[index]["latitude"], data[index]["longitude"] ]
          drawAlternativeRoutes(pointA, pointAlternative)
          drawAlternativeRoutes(pointAlternative, pointB)
        )
    )

  drawAlternativeRoutes = (pointA = null, pointB = null)->
    map.getRoutes({
      origin: pointA
      destination: pointB
      travelMode: 'driving'
      provideRouteAlternatives: false
      callback: (e)->
        $.each(e, (index, value)->
          colors = ["#fa253b", "#fa9e25", "#2c25fa", "#fa25c6", "#37fa25", "#eaff3e", "#fa2525", "#b8b8b8"]
          map.drawPolyline({
            path: e[index].overview_path
            strokeColor: colors[0]#'#131540'
            strokeOpacity: 0.6
            strokeWeight: 4
          });
        )
    })

  executeCaluculateRoutes = ->
    $(".js-get-alternative-route").on "click", (e)->
      calculateRoutes()
      $(this).addClass("hidden")
      $(".js-clean-alternative-route").removeClass("hidden")

  cleanAlternativeRoutes = ->
    $(".js-clean-alternative-route").on "click", (e)->
      map.removePolylines()
      map.removeMarkers()
      window.get_all_arroyo()
      $(this).addClass("hidden")
      clicks = 0
      location.reload();

  executeCaluculateRoutes()
  cleanAlternativeRoutes()

