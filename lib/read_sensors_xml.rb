module ReadSensorsXML
  module ClassMethods
    require 'nokogiri'

    def get_from_xml
      @doc = Nokogiri::XML(File.open("#{Rails.root}/public/sensors_xml.xml"))
      @doc.css("sensor").each do |sensor|
        serial = sensor.css("serial").text.gsub(" ", "")
        status = sensor.css("status").text.gsub(" ", "")
        latitude = sensor.css("latitude").text.gsub(" ", "")
        longitude = sensor.css("longitude").text.gsub(" ", "")
        puts serial.inspect
        find_by_external_id(serial).update_attribute(:status, status)
        sleep(3)
      end

    end
  end

  module InstanceMethods
  end

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end
