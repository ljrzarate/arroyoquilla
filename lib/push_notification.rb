module PushNotification
  module ClassMethods
  end

  module InstanceMethods
    require 'rest_client'
    def send_push_notification
      registration_ids = GcmUser.all.map!{|u| u.gcm_regid}
      begin
        response = RestClient.post(Settings.push_notification.url, { :registration_ids => registration_ids, :collapse_key => "Change in the arroyos app", :data => {:status=> self.status, :id=> self.id} }.to_json, :content_type=> "application/json", :"Authorization"=> Settings.push_notification.authorization_key)
        puts response
      rescue Exception => e
        puts e
      end
    end
  end

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end
