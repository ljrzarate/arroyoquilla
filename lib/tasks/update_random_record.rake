
namespace :auto_update_record do
  desc "This task will be execute every minute to update a random record to simulate change of state in some sensor"
  task :update_record=> :environment do |t, args|
    states = ["good", "bad", "medium"]
    all_sensor = Sensor.all
    20.times do
      sleep(2.5)
      random_record = all_sensor.sample
      clone_state = states.clone.delete_if {|state| state == random_record.status.to_s}
      if random_record.update_attributes(status: clone_state.sample)
        puts "Sensor with id: #{random_record.id} change status to: #{random_record.status}"
      else
        puts "Sensor with id: #{random_record.id} was not updated"
      end
    end
  end

end