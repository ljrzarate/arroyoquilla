class AddSensorIdToPoints < ActiveRecord::Migration
  def change
    add_column :points, :sensor_id, :integer
  end
end
