class AddAlvernativeFieldToPoints < ActiveRecord::Migration
  def change
    add_column :points, :alternative, :boolean
  end
end
