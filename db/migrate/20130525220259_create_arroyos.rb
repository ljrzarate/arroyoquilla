class CreateArroyos < ActiveRecord::Migration
  def change
    create_table :arroyos do |t|
      t.string :name
      t.string :address

      t.timestamps
    end
  end
end
