class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.float :latitude
      t.float :longitud
      t.integer :arroyo_id

      t.timestamps
    end
  end
end
