class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :status
      t.float :latitude
      t.float :longitud
      t.integer :arroyo_id

      t.timestamps
    end
  end
end
