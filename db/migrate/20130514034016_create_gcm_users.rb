class CreateGcmUsers < ActiveRecord::Migration
  def change
    create_table :gcm_users do |t|
      t.text :gcm_regid
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
