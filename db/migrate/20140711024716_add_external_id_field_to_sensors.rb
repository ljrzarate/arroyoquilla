class AddExternalIdFieldToSensors < ActiveRecord::Migration
  def change
    add_column :sensors, :external_id, :string
  end
end
