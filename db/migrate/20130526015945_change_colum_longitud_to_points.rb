class ChangeColumLongitudToPoints < ActiveRecord::Migration
  def up
    rename_column :points, :longitud, :longitude
    rename_column :sensors, :longitud, :longitude
  end

  def down
    rename_column :sensors, :longitude, :longitud
    rename_column :sensors, :longitude, :longitud
  end
end
