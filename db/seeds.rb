# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
arroyo = Arroyo.create(name: "Kr 59")

arroyo.sensors.create(latitude: 10.99269, longitude: -74.79347, status: "good")
arroyo.sensors.create(latitude: 10.99228, longitude: -74.7938, status: "bad")

arroyo.points.create(latitude: 10.99147, longitude: -74.79452)
arroyo.points.create(latitude: 10.99191, longitude: -74.79407)
arroyo.points.create(latitude: 10.99228, longitude: -74.7938)
arroyo.points.create(latitude: 10.99269, longitude: -74.79347)

